<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');
Route::get('/data-list', 'IndexController@tabel');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

//crud

//create
Route::get('/cast/create','CastController@create');

//input ke db
Route::post('/cast','CastController@store');

//read
Route::get('/cast','CastController@index');

//detail
Route::get('/cast/{cast_id}','CastController@show');

//edit
Route::get('/cast/{cast_id}/edit','CastController@edit');

//update
Route::put('/cast/{cast_id}','CastController@update');

//delete
Route::delete('/cast/{cast_id}','CastController@destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
