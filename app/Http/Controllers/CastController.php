<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{
    public function create()
    {
        return view('Cast.tambah');
    }
    public function store(request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $Cast = new Cast;
 
        $Cast->nama = $request->nama;
        $Cast->umur = $request->umur;
        $Cast->bio = $request->bio;
 
        $Cast->save();
        return redirect('/cast');  
    }
    public function index()
    {
        $Cast = Cast::all();
        return view('Cast.tampil', compact('Cast'));
    }
    public function show($id){
        $CastDetail = Cast::find($id);
        return view('Cast.detail',compact('CastDetail'));
    }
    public function edit($id){
        $CastEdit = Cast::find($id);
        return view('Cast.update',compact('CastEdit'));
    }
    public function update($id,request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $Cast = Cast::find($id);
 
        $Cast->nama = $request->nama;
        $Cast->umur = $request->umur;   
        $Cast->bio = $request->bio;
 
        $Cast->save();
        return redirect('/cast');
    }
    public function destroy($id){
        $CastDelete = Cast::find($id);
        $CastDelete->delete();
        return redirect('/cast');
    }
}