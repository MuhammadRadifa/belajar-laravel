@extends('layout.master')
@section('judul')
<h1>SanberBook</h1>
@endsection
@section('section')
<form action="/cast/{{$CastEdit->id}}" method="POST">
    @csrf
    @method("PUT")
    <div class="form-group">
      <label for="nama">nama</label>
      <input type="text" name="nama" class="form-control" id="text" placeholder="Masukkan Nama" value="{{$CastEdit->nama}}">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="umur">umur</label>
      <input type="number" name="umur" class="form-control" id="umur" placeholder="umur" value="{{$CastEdit->umur}}">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="bio">bio</label>
      <textarea name="bio" id="bio" cols="30" rows="10" class="form-control">{{$CastEdit->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection