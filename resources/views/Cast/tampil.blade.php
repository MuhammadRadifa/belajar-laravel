@extends('layout.master')
@section('judul')
<h1>Halaman List Kategori</h1>
@endsection
@section('section')

<a href="/cast/create" class="btn btn-primary btn-sm">tambah</a>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($Cast as $key => $item)
        <tr>
          <th scope="row">{{$key+1}}</th>
          <td>{{$item->nama}}</td>
          <td>{{$item->umur}}</td>
          <td>
              <form action="/cast/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm" >detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-success btn-sm" >Edit</a>
                <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
            </form>
        </td>
        </tr>
        @empty
            <div>
                <h2>
                    kosong
                </h2>
            </div>
        @endforelse
    </tbody>
</table>

@endsection