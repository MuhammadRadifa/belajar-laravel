@extends('layout.master')
@section('judul')
<h1>SanberBook</h1>
@endsection
@section('section')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label for="nama">nama</label>
      <input type="text" name="nama" class="form-control" id="text" placeholder="Masukkan Nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="umur">umur</label>
      <input type="number" name="umur" class="form-control" id="umur" placeholder="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="bio">bio</label>
      <textarea name="bio" id="bio" cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection