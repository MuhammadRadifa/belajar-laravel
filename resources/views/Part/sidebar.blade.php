<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="/" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Halaman
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="data-list" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Table Data</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="/" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                  film
              </p>
          </a>
      </li>
        <li class="nav-item">
          <a href="/" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                  Genre
              </p>
          </a>
      </li>
      {{-- logout --}}
      <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-th"></i>
                <p>
                  logout
              </p>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        
    </li>
    </ul>
  </nav>