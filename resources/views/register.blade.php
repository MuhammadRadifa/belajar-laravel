@extends('layout.master')
@section('judul')
<h1>Buat Account Baru</h1>
@endsection
@section('section')
<h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
      <label for="first">FirstName :</label><br /><br />
      <input type="text" name="first" /><br /><br />
      <label for="last">lastName :</label> <br /><br />
      <input type="text" name="last" /> <br />
      <br />
      <label for="gender">Gender :</label> <br />
      <br />
      <input type="radio" name="gender" id="gender" />Male <br />
      <input type="radio" name="gender" id="gender" />Female <br />
      <input type="radio" name="gender" id="gender" />Other <br /><br />
      <label for="National">Nationaly :</label><br /><br />
      <select name="National" id="National">
        <option value="Indonesia">Indonesia</option>
        <option value="Inggris">Inggris</option>
        <option value="Other">Other</option></select
      ><br />
      <br />
      <label for="bhs">Languege Spoken :</label><br /><br />
      <input type="checkbox" name="bhs" id="bhs" />Indonesia <br />
      <input type="checkbox" name="bhs" id="bhs" />English <br />
      <input type="checkbox" name="bhs" id="bhs" />Other <br />
      <br />
      <label for="bio">Bio :</label><br /><br />
      <textarea name="Bio" id="Bio" cols="30" rows="10"></textarea> <br />
      <input type="submit" value="Sign up" />
    </form>
@endsection
